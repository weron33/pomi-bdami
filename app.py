from fastapi import FastAPI

import pandas as pd
import sqlalchemy as db

from sqlalchemy_utils import database_exists, create_database

from routes import users, inna_tabela

app = FastAPI()

url = 'postgresql://postgres:postgres@localhost:5432/BDAMI-db'
conn = db.create_engine(url=url)
if not database_exists(conn.url):
    create_database(conn.url)


@app.get('/api/load/excel')
async def _load_excel(filepath: str):
    data = pd.read_excel(f'./data/{filepath}')
    print(data)
    data.to_sql('users', con=conn, if_exists='replace')
    return {'msg': 'Success'}


app.include_router(users.router)
app.include_router(inna_tabela.router)
