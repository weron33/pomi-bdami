import pandas as pd
import sqlalchemy as db
from sqlalchemy_utils import database_exists, create_database

url = 'postgresql://postgres:postgres@localhost:5432/BDAMI-db'
conn = db.create_engine(url=url)
if not database_exists(conn.url):
    create_database(conn.url)


class DBConnection:
    def __init__(self, table_name: str = 'users'):
        self.table_name = table_name

    def select(self, index=None):
        if index is None:
            obj = pd.read_sql(f'''SELECT * FROM {self.table_name}''', con=conn)
        else:
            obj = pd.read_sql(f'''SELECT * FROM {self.table_name} WHERE index={index};''', con=conn)
        return obj
    
    def append(self, obj: pd.DataFrame):
        obj.to_sql(self.table_name, con=conn, if_exists='append')
        return True
    
    def replace(self, df: pd.DataFrame):
        df.to_sql(self.table_name, con=conn, if_exists='replace', index=False)
        return True
