import pandas as pd
from fastapi import applications
from models.users import DBConnection

DBconn = DBConnection('users')


def get_user(index: int) -> pd.DataFrame:
    user = DBconn.select(index)
    return user


def post_user(user: dict) -> bool:
    user = pd.DataFrame(user)
    return DBconn.append(user)


def put_user(user: dict, index: int) -> bool:
    users = DBconn.select()
    for column in user:
        users[column].loc[index] = user[column]
    return DBconn.replace(users)


def delete_user(index: int) -> bool:
    users = DBconn.select()
    users = users.drop(index=index)
    return DBconn.replace(users)
