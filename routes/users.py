from fastapi import APIRouter, Request

from services import users
from schemas.reseposes import Response

response = Response()
router = APIRouter(prefix='/api/users')


@router.get('user/{index}')
async def _get_user(index: int) -> list:
    user = users.get_user(index)
    return user.to_dict(orient='records')


@router.post('/api/user')
async def _post_user(request: Request) -> dict:
    body = await request.json()
    user = body['user']
    if users.post_user(user):
        return response.CREATED
    else:
        return response.CONFLICT


@router.put('/api/user/{index}')
async def _put_user(request: Request, index: int) -> dict:
    body = await request.json()
    user = body['user']
    if users.put_user(user, index):
        return response.OK
    else:
        return response.CONFLICT


@router.delete('/api/user/{index}')
async def _delete_user(index: int) -> dict:
    if users.delete_user(index):
        return response.OK
    else:
        return response.CONFLICT
