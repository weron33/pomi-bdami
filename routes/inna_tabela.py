from http import HTTPStatus

import pandas as pd
from fastapi import APIRouter, Request

import sqlalchemy as db
from sqlalchemy_utils import database_exists, create_database

url = 'postgresql://postgres:postgres@localhost:5432/BDAMI-db'
conn = db.create_engine(url=url)
if not database_exists(conn.url):
    create_database(conn.url)

router = APIRouter()


@router.get('/api/inna_tabela/{index}')
async def _get_inna_tabela(index: int) -> list:
    inna_tabela = pd.read_sql(f'''SELECT * FROM inna_tabela WHERE "inna_tabela"."customerId"={index};''', con=conn)
    return inna_tabela.to_dict(orient='records')


@router.post('/api/inna_tabela')
async def _post_inna_tabela(request: Request) -> dict:
    body = await request.json()
    inna_tabela = body['inna_tabela']
    inna_tabela = pd.DataFrame(inna_tabela)
    inna_tabela.to_sql('inna_tabela', con=conn, if_exists='append')
    return {"msg": HTTPStatus.CREATED.phrase, "code": HTTPStatus.CREATED}


@router.put('/api/inna_tabela/{index}')
async def _put_inna_tabela(request: Request, index: int) -> dict:
    body = await request.json()
    inna_tabela_dict = body['inna_tabela']
    columns = [key for key in inna_tabela_dict]
    inna_tabela = pd.read_sql('''SELECT * FROM inna_tabela''', con=conn)
    for column in columns:
        print(column)
        inna_tabela[column].loc[index] = inna_tabela_dict[column]
    print(inna_tabela)
    inna_tabela.to_sql('inna_tabela', con=conn, if_exists='replace', index=False)
    return {"msg": HTTPStatus.OK.phrase, "code": HTTPStatus.OK}


@router.delete('/api/inna_tabela/{index}')
async def _delete_inna_tabela(index: int) -> dict:
    inna_tabela = pd.read_sql('''SELECT * FROM inna_tabela''', con=conn)
    inna_tabela = inna_tabela.drop(index=index)
    inna_tabela.to_sql('inna_tabela', con=conn, if_exists='replace', index=False)
    return {"msg": HTTPStatus.OK.phrase, "code": HTTPStatus.OK}



