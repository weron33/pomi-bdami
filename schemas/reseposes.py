from http import HTTPStatus


class Response:
    def __init__(self):
        self.OK = {"msg": HTTPStatus.OK.phrase, "code": HTTPStatus.OK}
        self.CREATED = {"msg": HTTPStatus.CREATED.phrase, "code": HTTPStatus.CREATED}
        self.CONFLICT = {"msg": HTTPStatus.CONFLICT.phrase, "code": HTTPStatus.CONFLICT}